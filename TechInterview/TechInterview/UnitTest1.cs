using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using TechInterview.pages;

namespace TechInterview
{
    public class Tests
    {
        private ProductPage productPage;
        private CartPage CartPage;
        private HomePage homePage;
        private SearchResultsPage searchResults;
        IWebDriver driver;
        [SetUp]
        public void Setup()
        {
            driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("http://34.205.174.166/");
            driver.Manage().Window.Maximize();
        }


        [Test(Description = "Validate that a product can be added to the shopping cart")]
        public void Test1()
        {
            //step 1
            driver.Navigate().GoToUrl("http://34.205.174.166/product/oscarfonseca/");
            productPage = new ProductPage(this.driver);
            //Verify expected results
            Assert.AreEqual("oscarfonseca  QA Playground", productPage.GetPageTitle(), "Incorrect page found", null);
            Assert.AreEqual("oscarfonseca", productPage.ProductTitle.Text, "Incorrect product title found", null);
            Assert.IsTrue(productPage.Price.Displayed, "Pricing is not displayed", null);
            //step 2
            productPage.TypeOnWebElement(productPage.QuantityInput, "7");
            //Verify expected results
            Assert.AreEqual("7", productPage.QuantityInput.GetAttribute("value"), "the element doesn't show the typed text", null);
            //step 3
            productPage.AddCart.Submit();
            //Verify expected results
            productPage.CartRefresh(driver);
            Assert.AreEqual("$175.00 7 items", productPage.Cart.Text, "Incorrect amount shown in Cart", null);
            //step 4
            productPage.Cart.Click();
            //Verify expected results
            CartPage = new CartPage(driver);
            Assert.AreEqual("Cart  QA Playground", CartPage.GetPageTitle(), "Incorrect page found", null);
            Assert.AreEqual("oscarfonseca", CartPage.ProductName.Text, "the product is not correct", null);
            Assert.AreEqual("$25.00", CartPage.ProductPrice.Text, "the unit price is not correct", null);
            Assert.AreEqual("7", CartPage.ProductQuantity.GetAttribute("value"), "the quantity is not correct", null);
        }

        [Test (Description = "Verify that users can search for Hoodie products and navigate to the detail page")]
        public void Test2()
        {
            //step 1
            homePage = new HomePage(driver);
            //Verify expected results
            Assert.AreEqual("QA Playground  Just another WordPress site", homePage.GetPageTitle(), "Incorrect page found", null);
            Assert.IsTrue(homePage.SearchInput.Displayed, "Search element is not present", null);
            //step 2
            homePage.TypeOnWebElement(homePage.SearchInput, "Hoodie");
            //Verify expected results
            Assert.AreEqual("Hoodie", homePage.SearchInput.GetAttribute("value"), "the element doesn't show the typed text", null);
            //step 3
            homePage.SearchInput.SendKeys(Keys.Enter);
            searchResults = new SearchResultsPage(driver);
            //Verify expected results
            Assert.AreEqual("Search results: Hoodie", searchResults.SearchResultTitle.Text, "incorrect results page loaded", null);
            Assert.AreEqual(4, searchResults.Results.Count, "amount of results is incorrect", null);
            Assert.IsTrue(searchResults.IsResultPresent("Hoodie with Pocket"));
            //step 4
            searchResults.LikedResult("Hoodie with Pocket").Click();
            productPage = new ProductPage(this.driver);
            //Verify expected results
            Assert.AreEqual("Hoodie with Pocket  QA Playground", productPage.GetPageTitle(), "Incorrect page found", null);
            Assert.AreEqual("Hoodie with Pocket", productPage.ProductTitle.Text, "Incorrect product title found", null);
        }

        [Test(Description = "Validate that a discount can be applied")]
        public void Test3()
        {
            //step 1
            driver.Navigate().GoToUrl("http://34.205.174.166/product/oscarfonseca/");
            productPage = new ProductPage(this.driver);
            //Verify expected results
            Assert.AreEqual("oscarfonseca  QA Playground", productPage.GetPageTitle(), "Incorrect page found", null);
            Assert.AreEqual("oscarfonseca", productPage.ProductTitle.Text, "Incorrect product title found", null);
            //step 2
            productPage.AddCart.Submit();
            //Verify expected results
            productPage.CartRefresh(driver);
            Assert.AreEqual("$25.00 1 item", productPage.Cart.Text, "Incorrect amount shown in Cart", null);
            //step 3
            productPage.Cart.Click();
            //Verify expected results
            CartPage = new CartPage(driver);
            Assert.AreEqual("Cart  QA Playground", CartPage.GetPageTitle(), "Incorrect page found", null);
            Assert.AreEqual("oscarfonseca", CartPage.ProductName.Text, "the product is not correct", null);
            Assert.AreEqual("$25.00", CartPage.ProductPrice.Text, "the unit price is not correct", null);
            Assert.AreEqual("1", CartPage.ProductQuantity.GetAttribute("value"), "the element doesn't show the typed text", null);
            //step 4
            CartPage.TypeOnWebElement(CartPage.CuponCode, "100off_oscar");
            //Verify expected results
            Assert.AreEqual("100off_oscar", CartPage.CuponCode.GetAttribute("value"), "the element doesn't show the typed text", null);
            //step 5
            CartPage.ApplyCupon.Submit();
            //Verify expected results
            Assert.IsTrue(CartPage.IsCuponApplied("Coupon: 100off_oscar", driver),"cupon was not applied", null);
        }

        [TearDown]
        public void Teardown()
        {
            driver.Close();
        }
    }
}