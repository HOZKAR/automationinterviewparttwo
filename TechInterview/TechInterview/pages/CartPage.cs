﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TechInterview.pages
{
    class CartPage
    {
        private IWebDriver driver;

        public CartPage(IWebDriver driver)
        {
            this.driver = driver;
            ProductName = this.driver.FindElement(By.CssSelector("td.product-name"));
            ProductPrice = this.driver.FindElement(By.CssSelector("td.product-price"));
            ProductQuantity = this.driver.FindElement(By.CssSelector(".input-text.qty.text"));
            CuponCode = this.driver.FindElement(By.Name("coupon_code"));
            ApplyCupon = this.driver.FindElement(By.Name("apply_coupon"));
        }

        public IWebElement ProductName { get; set; }

        public IWebElement ProductPrice { get; set; }

        public IWebElement ProductQuantity { get; set; }

        public IWebElement CuponCode { get; set; }

        public IWebElement ApplyCupon { get; set; }
        public string GetPageTitle()
        {
            return driver.Title.ToString();
        }
        public void TypeOnWebElement(IWebElement element, string textToType)
        {
            element.Clear();
            element.SendKeys(textToType);
        }

        public Boolean IsCuponApplied(String cupon, IWebDriver driver)
        {
            if(driver.FindElement(By.ClassName("cart-discount")).Enabled && driver.FindElement(By.CssSelector("tr.cart-discount th:first-child")).Text.Equals(cupon))
            {
                return true;
            }
            return false;
        }
    }
}
