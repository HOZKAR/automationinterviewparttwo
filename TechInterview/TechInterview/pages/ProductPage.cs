﻿using OpenQA.Selenium;

namespace TechInterview.pages
{
    class ProductPage
    {
        private IWebDriver driver;

        public ProductPage(IWebDriver driver)
        {
            this.driver = driver;
            ProductTitle = this.driver.FindElement(By.CssSelector(".product_title.entry-title"));
            QuantityInput = this.driver.FindElement(By.CssSelector(".input-text.qty.text"));
            Price = this.driver.FindElement(By.CssSelector(".woocommerce-Price-amount.amount"));
            AddCart = this.driver.FindElement(By.Name("add-to-cart"));
            Cart = this.driver.FindElement(By.CssSelector(".cart-contents"));
        }

        public IWebElement ProductTitle { get; set; }

        public IWebElement QuantityInput { get; set; }

        public IWebElement Price { get; set; }

        public IWebElement AddCart { get; set; }

        public IWebElement Cart { get; set; }
        public string GetPageTitle()
        {
            return driver.Title.ToString();
        }
        public void TypeOnWebElement(IWebElement element, string textToType)
        {
            element.Clear();
            element.SendKeys(textToType);
        }
        public void CartRefresh(IWebDriver driver)
        {
            Cart = driver.FindElement(By.CssSelector(".cart-contents"));
        }
    }
}
