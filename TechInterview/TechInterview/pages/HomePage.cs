﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TechInterview.pages
{
    class HomePage
    {
        private IWebDriver driver;

        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
            SearchInput = this.driver.FindElement(By.Id("woocommerce-product-search-field-0"));
        }

        public IWebElement SearchInput { get; set; }

        public string GetPageTitle()
        {
            return driver.Title.ToString();
        }

        public void TypeOnWebElement(IWebElement element, string textToType)
        {
            element.Clear();
            element.SendKeys(textToType);
        }
    }
}
