﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TechInterview.pages
{
    class SearchResultsPage
    {
        private IWebDriver driver;

        public SearchResultsPage(IWebDriver driver)
        {
            this.driver = driver;
            SearchInput = this.driver.FindElement(By.Id("woocommerce-product-search-field-0"));
            SearchResultTitle = this.driver.FindElement(By.CssSelector(".woocommerce-products-header__title.page-title"));
            Results = driver.FindElements(By.CssSelector("li.product.type-product a:first-child"));
            ResultsTextsOnly = driver.FindElements(By.ClassName("woocommerce-loop-product__title"));
        }

        public IWebElement SearchInput { get; set; }
        public IWebElement SearchResultTitle { get; set; }
        public IList<IWebElement> Results { get; set; }
        private IList<IWebElement> ResultsTextsOnly;

        public string GetPageTitle()
        {
            return driver.Title.ToString();
        }

        public void TypeOnWebElement(IWebElement element, string textToType)
        {
            element.Clear();
            element.SendKeys(textToType);
        }

        public Boolean IsResultPresent(string element)
        {
            for(int i = 0; i < ResultsTextsOnly.Count; i++)
            {
                if (ResultsTextsOnly[i].Text.Equals(element))
                {
                    return true;
                }
            }
            return false;
        }

        public IWebElement LikedResult(string element)
        {
            for (int i = 0; i < ResultsTextsOnly.Count; i++)
            {
                if (ResultsTextsOnly[i].Text.Equals(element))
                {
                    return Results[i];
                }
            }
            return null;
        }
    }
}
